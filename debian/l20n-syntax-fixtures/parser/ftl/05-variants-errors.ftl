key1 =
 [:key] Value

key2 = Value

key3 =
 [] Value

key4 =
 **[f] Foo


key5 =
 *fw] Foo

key6 =
  [ a] A

key7 =
  [ x/a] XA

key8 =
  [x y/a] XYA
